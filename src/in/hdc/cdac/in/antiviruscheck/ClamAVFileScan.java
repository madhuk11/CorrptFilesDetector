/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.hdc.cdac.in.antiviruscheck;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import net.taldius.clamav.ClamAVScanner;
import net.taldius.clamav.ClamAVScannerFactory;

/**
 *
 * @author Madhusudhan <madhusudhankk@cdac.in>
 */
public class ClamAVFileScan {
    
 private ClamAVScanner scanner;
 private File file;
 
 public ClamAVFileScan(File file){
     this.file = file;
 }
 
 public boolean scanResult() {
     
  boolean isInfected=false;
  
  try {
   initScanner();
   isInfected= fileScanner(file.getAbsolutePath());
 } catch (Exception e) {
   e.printStackTrace();
  }
  return isInfected;
}

       /**
        * Method to initialize clamAV scanner
        */
       public void initScanner() {
              try{
              ClamAVScannerFactory.setClamdHost("10.208.34.121"); // Host ip where 'clamd' process is running
              ClamAVScannerFactory.setClamdPort(3310); // Port on which 'clamd' process is listening
              ClamAVScannerFactory.setConnectionTimeout(20);// Connection time out to connect 'clamd' process
              this.scanner = ClamAVScannerFactory.getScanner();
              }
              catch(Exception ex){
                  ex.printStackTrace();
              }
       }

        /**
        * Method to scans files to check whether file is virus infected
        *
        * @param fileInputStream
        * @return
        * @throws Exception
        */
       public boolean fileScanner(String fileInputStream) throws Exception {

              boolean resScan = false;
              if (fileInputStream != null) {
                 InputStream inputfile=new FileInputStream(fileInputStream);
                     resScan = scanner.performScan(inputfile);
                    
              } else {

                     throw new Exception();
              }
              return resScan;
       }

}