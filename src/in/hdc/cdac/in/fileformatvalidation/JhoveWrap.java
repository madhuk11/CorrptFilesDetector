/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.hdc.cdac.in.fileformatvalidation;

import edu.harvard.hul.ois.jhove.*;
import java.io.File;

/**
 *
 * @author Madhusudhan <madhusudhankk@cdac.in>
 */
public class JhoveWrap extends JhoveBase {
    
     /**
     * Wrapper class constructor.
     */
    public JhoveWrap()
            throws JhoveException {
        super();
    }
    
    /**
     * Invoke the protected processFile() method of the parent JhoveBase class.
     *
     * @param app Application object
     * @param module JHOVE module
     * @param verbose Verbosity flag, if true produce maximum amount of
     * representation information
     * @param file The formatted file being validated
     * @param info Representation information about the file
     * @return False if the module cannot perform validation
     */
    @Override
    public boolean processFile(App app, Module module, boolean verbose, File file, RepInfo info)
            throws Exception {
         
        return super.processFile(app, module, verbose, file, info);
    }
    
    
}
