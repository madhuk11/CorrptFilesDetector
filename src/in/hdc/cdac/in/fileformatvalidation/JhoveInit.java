/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.hdc.cdac.in.fileformatvalidation;

import edu.harvard.hul.ois.jhove.App;
import edu.harvard.hul.ois.jhove.Module;
import edu.harvard.hul.ois.jhove.RepInfo;
import java.io.File;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author Madhusudhan <madhusudhankk@cdac.in>
 */
public class JhoveInit {
    
    /*
     * Descriptive metadata about the application.
     */
    private static final String APP = "SipValidation";
    private static final String RELEASE = "1.0";
    private static final int[] DATE = {2016, 10, 05};
    private static final String USAGE = "java " + APP + " file [module]";
    private static final String RIGHTS = "Copyright 2016 by CDAC-PUNE";
    
    /*
     * JHOVE configuration file.
     */
    
    private static final String CONFIG = JhoveInit.class.getResource("/in/hdc/cdac/in/fileformatvalidation/config/jhove.conf").getFile();
    
    public RepInfo valid(File file, String modulename, String config) {
    
         RepInfo info = null;

        try {
            
            /*
             * instantiate an App object with information about the application.
             */
            
            App app = new App(APP, RELEASE, DATE, USAGE, RIGHTS);


            /*
             * Instantiate the JHOVE engine. Normally, we would instantiate the
             * JhoveBase class directly. However, because we want access to the
             * protected processFile() method, we have to use the JhoveWrap
             * wrapper class. The engine is initialized from the JHOVE
             * configuration file.
             */

            JhoveWrap je = new JhoveWrap();
            je.init(config, null);
            Module module = null;
            module = je.getModule(modulename);

            /*
             * Instantiate a RepInfo object to hold the representation
             * information about the file.
             */
            info = new RepInfo(file.toString());
            if (module != null) {

                /*
                 * If a particular module has been specified, invoke it to
                 * validate the file.
                 */
                if (!je.processFile(app, module, false, file, info)) {

                    System.err.println("Module does not supportvalidation");
                }
            } else {
                /*
                 * If no module has been specified, iteratively invoke all
                 * configured modules until one indicates that the file is
                 * well-formed.
                 */
                List list = je.getModuleList();
                Iterator iter = list.iterator();

                while (iter.hasNext()) {

                    module = (Module) iter.next();
                    RepInfo clone = (RepInfo) info.clone();

                    if (je.processFile(app, module, false, file, clone)) {
                        if (clone.getWellFormed() == RepInfo.TRUE) {
                            info.copy(clone);
                            break;
                        }
                    }
                }
            }

            /*
             * The given module has determined the file to be well-formed, and
             * possibly valid. The getWellFormed() and getValid() methods return
             * 1 if TRUE, 0 if FALSE, and -1 if UNDETERMINED.
             */

        } catch (Exception e) {
            e.printStackTrace(System.err);

        }
        return info;
    }
    
    public boolean init(File file){
        boolean isvalid = false;
        try{
            JhoveInit j = new JhoveInit();
            isvalid = j.checkFileValid(file);
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
        return isvalid;
    }
    
    public boolean checkFileValid(File file){
                String filename = file.getName();
                String[] split = filename.split("\\.");
                String fileExtension = split[split.length - 1];
                RepInfo info = valid(file, fileExtension+"-hul", CONFIG);
                
                if(info.getWellFormed()==1 && info.getValid()==1){
                    return true;
                }
                else {
                    return false;
                }
                
    }
    
}
