/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.hdc.cdac.in.tablemodel;

import in.hcdc.cdac.detectfiles.CorruptedFileInfo;
import in.hcdc.cdac.detectfiles.IdentifyCorruptedFiles;
import in.hcdc.cdac.ui.OutputPathPanel;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Madhusudhan <madhusudhankk@cdac.in>
 */
public final class FilesListTableModel extends AbstractTableModel {

    private String[] columnNames = {"Select", "File Name", "File Path", "File Size (MB)", "Is Valid", "Virus check"};
    private Object[][] data = null;
    ButtonGroup btnGroup;
    private OutputPathPanel card;
    
    
    public DefaultTableModel getModel() {
        return model;
    }

    public void setModel(DefaultTableModel model) {
        this.model = model;
    }
        
    public FilesListTableModel(String path){
        addFilesToTable(path);
    }
        
        

    public void addFilesToTable(String path) {
        try {
             int maxThreads = 10;
            IdentifyCorruptedFiles icf = new IdentifyCorruptedFiles(maxThreads);
            ExecutorService executor = Executors.newFixedThreadPool(maxThreads);
         //   String basePath = outputPathPanel.getTxtOutputPath().getText();
          String basePath = path;
            List<Future<ArrayList<CorruptedFileInfo>>> listfiles = icf.detectFiles(basePath, executor);
            int totalfiles = 0;
            //System.out.println("--list file len: "+listfiles.size());            
            for(int i=0;i<listfiles.size();i++){
               // System.out.println(i+" future obj len: "+listfiles.get(i).get().size());
                totalfiles= totalfiles + (listfiles.get(i).get().size());
            }
          //  System.out.println("--total files--"+totalfiles);            
            data = new Object[totalfiles][columnNames.length];
           
            if (listfiles.size() > 0) {
                for (Future<ArrayList<CorruptedFileInfo>> future : listfiles) {  
                    try {
                        // System.out.println(future.get().size());
                        //     int setLength = future.get().size();
                        
                        if (!future.get().isEmpty()) {
                            for (CorruptedFileInfo info : future.get()) {
                                model.addRow(new Object[] {
                                    new Boolean(false),
                                    info.getFileName(),
                                    info.getFilePath(),
                                    info.getFileSize(),
                                    info.getFileFormatValid(),
                                    info.getVirusCheckStatus()
                                });
                            }
                        }

                    } catch (InterruptedException | ExecutionException e) {
                        e.printStackTrace();
                    }
                }
                executor.shutdown();
            }
////            btnGroup = new ButtonGroup();
//            for (int i = 0; i < data.length; i++) {
//                ((JCheckBox) getValueAt(i, 0)).addActionListener(new ActionListener() {
//                    @Override
//                    public void actionPerformed(ActionEvent e) {
//                        System.out.println("in event " + e.getActionCommand());
//                    }
//                });
//            }
        } catch (Exception ex) {
            ex.getMessage();
            ex.printStackTrace();
//            Logger.getInstance().logMessage(ex);
        }
    }

    @Override
    public int getRowCount() {
        return data.length;
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    private DefaultTableModel model = new DefaultTableModel(null, columnNames) {
        @Override public Class<?> getColumnClass(int column) {
          return (column==0) ? Boolean.class : Object.class;
        }
        @Override
            public boolean isCellEditable(int row, int column) {
               //all cells false
               boolean isEditable;
               if(column ==0){
                   isEditable = true;
               }
               else {
                   isEditable = false;
               }
                return isEditable;
            }
      };
    
//         public Class getColumnClass(int c) {
//            return getValueAt(0, c).getClass();
//        }
 

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        return data[rowIndex][columnIndex];
    }

    @Override
    public String getColumnName(int column) {
        return columnNames[column];
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        if(column==0){
            return true;
        }
        else {
            return false;
        }
    }

    public boolean isRowSelected(int row) {
        return ((JCheckBox) getValueAt(row, 0)).isSelected();
    }

    public String getSelectedRecord(int selectedRow) {
        return (String) getValueAt(selectedRow, 1);
    }

    public OutputPathPanel getCard() {
        return card;
    }

    public String[] getColumnNames() {
        return (String[]) columnNames.clone();
    }

    public void setColumnNames(String[] columnNames) {
        this.columnNames = (String[]) columnNames.clone();
    }

    public Object[][] getData() {
        return (Object[][]) data.clone();
    }

    public void setData(Object[][] data) {
        this.data = (Object[][]) data.clone();
    }
    
}
