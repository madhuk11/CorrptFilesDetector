/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.hcdc.cdac.logger;

/**
 *
 * @author Madhusudhan <madhusudhankk@cdac.in>
 */
public class LoggingException extends Exception {

    public LoggingException() {
        super();
    }

    public LoggingException(String msg) {
        super(msg);
    }
}
