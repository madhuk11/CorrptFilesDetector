/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.hcdc.cdac.main;

import static in.hcdc.cdac.constants.Constants.*;
import in.hcdc.cdac.logger.Logger;
import in.hcdc.cdac.ui.IDEMenubar;
import in.hcdc.cdac.ui.event.IDEEvent;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.net.URL;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

/**
 *
 * @author Madhusudhan <madhusudhankk@cdac.in>
 */
public class IDE {
    
    private static IDEMenubar menubar;
    private static IDECardsHolder cardsHolder;
    public static JFrame frame;

    public static void main(String[] args) {

        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                IDE.createAndShowIDE();
            }
        });

    }

    /**
     * Method to populate main window
     */
    
    public static void createAndShowIDE() {
        frame = new JFrame(APPLICATION_NAME);

        try {
            // adding menu bar to ide
            menubar = new IDEMenubar();

            URL imgURL = IDE.class.getResource("/in/hcdc/cdac/resources/images/coedp_logo.png");
            frame.setIconImage(new ImageIcon(imgURL).getImage());

            //initLoggers(configDirectory.getPath());
            frame.setJMenuBar(menubar);
            Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
            Dimension desiredDimension = new Dimension(dim.width, dim.height - 50);
            frame.setSize(desiredDimension);
           // setting location of IDE
            frame.setLocationRelativeTo(null);
            // adding contents to ide
            cardsHolder = new IDECardsHolder();
            frame.setContentPane(cardsHolder);
            frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
            IDEEvent menuEventHandler = new IDEEvent(cardsHolder);
            menubar.getMenuItemExit().addActionListener(menuEventHandler);
            frame.addWindowListener(new WindowAdapter() {
                @Override
                public void windowClosing(WindowEvent e) {
                    int option = JOptionPane.showConfirmDialog(frame, "Are you sure you want to quit?", "Exit Dialog", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
                    if (option == JOptionPane.YES_OPTION) {
                        // Helper.deleteConfigFile();
                        System.exit(0);
                    } else {
                    }
                }
            });
           frame.setVisible(true);
            
        } catch (Exception exception) {
            exception.printStackTrace();
            Logger.getInstance().logMessage(exception);
        }
    }
}
