package in.hcdc.cdac.ui;

import static in.hcdc.cdac.constants.Constants.*;
import java.awt.Color;
import java.awt.Dimension;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

/**
 *
 * @author Madhusudhan <madhusudhankk@cdac.in>
 */
public class IDEMenubar extends JMenuBar {

    //Menus
    private JMenu menuSIP, menuMapping, menuDatabase, menuOption, menuWindow;
    //MenuItems
    private JMenuItem menuItemShowDashboard, menuItemHelp, menuItemExit;

    public JMenuItem getMenuItemShowDashboard() {
        return menuItemShowDashboard;
    }

    public void setMenuItemShowDashboard(JMenuItem menuItemShowDashboard) {
        this.menuItemShowDashboard = menuItemShowDashboard;
    }

    public JMenuItem getMenuItemHelp() {
        return menuItemHelp;
    }

    public void setMenuItemHelp(JMenuItem menuItemHelp) {
        this.menuItemHelp = menuItemHelp;
    }

    public JMenuItem getMenuItemExit() {
        return menuItemExit;
    }

    public void setMenuItemExit(JMenuItem menuItemExit) {
        this.menuItemExit = menuItemExit;
    }

    public JMenu getMenuMapping() {
        return menuMapping;
    }

    public void setMenuMapping(JMenu menuMapping) {
        this.menuMapping = menuMapping;
    }

    public JMenu getMenuSIP() {
        return menuSIP;
    }

    public JMenu getMenuDatabase() {
        return menuDatabase;
    }

    public void setMenuDatabase(JMenu menuDatabase) {
        this.menuDatabase = menuDatabase;
    }

    public IDEMenubar() {
        setBackground(Color.decode("#9cbbdb"));
        setPreferredSize(new Dimension(100, 28));
        initialize();
        addMenus();
    }

    private void initialize() {
        //WINDOW
        menuWindow = new JMenu(MENU_BASIC_CONFIG);
        menuItemShowDashboard = new JMenuItem(MENU_ITEM_SHOW_DASHBOARD);
        menuItemHelp = new JMenuItem(MENU_ITEM_HELP);
        menuItemExit = new JMenuItem(MENU_ITEM_EXIT);
//        menuWindow.setMnemonic(Constants.KEY_WINDOW);
        menuItemShowDashboard.setAccelerator(KEY_SHOW_DASHBOARD);
        menuItemHelp.setAccelerator(KEY_HELP);
        menuItemExit.setAccelerator(KEY_EXIT);
    }

    private void addMenus() {

        menuWindow.add(menuItemShowDashboard);
        menuWindow.add(menuItemHelp);
        menuWindow.add(menuItemExit);

        add(menuWindow);
    }
}
