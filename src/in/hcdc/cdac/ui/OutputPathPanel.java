/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.hcdc.cdac.ui;

import in.hcdc.cdac.main.IDECardsHolder;
import in.hcdc.cdac.ui.event.MenuPanelMouseEvent;
import in.hcdc.cdac.util.Helper;
import in.hdc.cdac.in.tablemodel.FilesListTableModel;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.File;
import java.util.Arrays;
import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.RowFilter;
import javax.swing.border.TitledBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import net.miginfocom.swing.MigLayout;

/**
 *
 * @author Madhusudhan <madhusudhankk@cdac.in>
 */
public class OutputPathPanel extends JPanel {

    private FilesListTableModel fileslisttablemodel;
    private MenuPanel menuPanel;
    private IDECardsHolder ideCardsHolder;
    private JTable tableList;
    private JLabel lblOutPutPathDetails, lblOutputPath, lblSearch;
    private JTextField txtOutputPath, txtSearchTerm;
    private JButton btnBrowse, btnSave, btnReset, btnCancel, btnSubmit, btnMove;
    private JFileChooser fileDialog;
    private JPanel pathPanel, contentPanel, tablepanel, panelContent;
    private TableRowSorter sorter;
    
    public FilesListTableModel getFilesListTableModel1() {
        return fileslisttablemodel;
    }
    
    public JPanel getPanelContent() {
        return panelContent;
    }

    public void setPanelContent(JPanel panelContent) {
        this.panelContent = panelContent;
    }
    
    public JButton getBtnMove() {
        return btnMove;
    }

    public void setBtnMove(JButton btnMove) {
        this.btnMove = btnMove;
    }
    
    public void setFilesListTableModel(FilesListTableModel fileslisttablemodel) {
        this.fileslisttablemodel = fileslisttablemodel;
    }

    public JTable getTableList() {
        return tableList;
    }

    public void setTableList(JTable tableList) {
        this.tableList = tableList;
    }

    public JPanel getContentPanel() {
        return contentPanel;
    }

    public void setContentPanel(JPanel contentPanel) {
        this.contentPanel = contentPanel;
    }

    public JPanel getTablepanel() {
        return tablepanel;
    }

    public void setTablepanel(JPanel tablepanel) {
        this.tablepanel = tablepanel;
    }

    public MenuPanel getMenuPanel() {
        return menuPanel;
    }

    public void setMenuPanel(MenuPanel menuPanel) {
        this.menuPanel = menuPanel;
    }

    public IDECardsHolder getIdeCardsHolder() {
        return ideCardsHolder;
    }

    public void setIdeCardsHolder(IDECardsHolder ideCardsHolder) {
        this.ideCardsHolder = ideCardsHolder;
    }

    public JLabel getLblOutPutPathDetails() {
        return lblOutPutPathDetails;
    }

    public void setLblOutPutPathDetails(JLabel lblOutPutPathDetails) {
        this.lblOutPutPathDetails = lblOutPutPathDetails;
    }

    public JLabel getLblOutputPath() {
        return lblOutputPath;
    }

    public void setLblOutputPath(JLabel lblOutputPath) {
        this.lblOutputPath = lblOutputPath;
    }

    public JTextField getTxtOutputPath() {
        return txtOutputPath;
    }

    public void setTxtOutputPath(JTextField txtOutputPath) {
        this.txtOutputPath = txtOutputPath;
    }

    public JButton getBtnBrowse() {
        return btnBrowse;
    }

    public void setBtnBrowse(JButton btnBrowse) {
        this.btnBrowse = btnBrowse;
    }

    public JButton getBtnSave() {
        return btnSave;
    }

    public void setBtnSave(JButton btnSave) {
        this.btnSave = btnSave;
    }

    public JButton getBtnReset() {
        return btnReset;
    }

    public void setBtnReset(JButton btnReset) {
        this.btnReset = btnReset;
    }

    public JButton getBtnCancel() {
        return btnCancel;
    }

    public void setBtnCancel(JButton btnCancel) {
        this.btnCancel = btnCancel;
    }

    public JButton getBtnSubmit() {
        return btnSubmit;
    }

    public void setBtnSubmit(JButton btnSubmit) {
        this.btnSubmit = btnSubmit;
    }

    public JFileChooser getFileDialog() {
        return fileDialog;
    }

    public void setFileDialog(JFileChooser fileDialog) {
        this.fileDialog = fileDialog;
    }

    public JPanel getPathPanel() {
        return pathPanel;
    }

    public void setPathPanel(JPanel pathPanel) {
        this.pathPanel = pathPanel;
    }

    public OutputPathPanel(MenuPanel menuPanel, IDECardsHolder ideCardsHolder) {
        this.menuPanel = menuPanel;
        this.ideCardsHolder = ideCardsHolder;
        this.setSize(200, 300);
        initComponents();
        layoutComponents();
    }
    
    private void initComponents() {
        fileDialog = new JFileChooser();
        fileDialog.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        JSeparator separator = new JSeparator();
        separator.setForeground(Color.decode("#d1d1d1"));
        
        //row 1
        lblOutPutPathDetails = new JLabel(Helper.getFormattedPageTitle("One-Time Configuration"));
        //row 2
        pathPanel = new JPanel(new MigLayout("insets 2", "[][]", "[]15[]15[]"));
        pathPanel.setBorder(BorderFactory.createRaisedSoftBevelBorder());
        lblOutputPath = new JLabel("Output Path");
        txtOutputPath = new JTextField();
        txtOutputPath.setEditable(false);
        btnSave = new JButton("Save");

        btnCancel = new JButton("Cancel");
        btnBrowse = new JButton("Browse");

        btnSubmit = new JButton("Submit");
        btnReset = new JButton("Reset");

        btnMove = new JButton("Move selected");
 
        pathPanel = new JPanel(new MigLayout("insets 7", "[][]", "[]"));
        pathPanel.setBorder(BorderFactory.createRaisedSoftBevelBorder());

//        btnBrowse.setEnabled(false);
        pathPanel.add(lblOutputPath, "align center,split 2,width 80!,height 25!");
        pathPanel.add(txtOutputPath, "growx,width 25%:30%:35%,height 25!");
        pathPanel.add(btnBrowse, "growx,wrap,width 80!,height 25!");
        btnBrowse.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                int openChoice = fileDialog.showOpenDialog(ideCardsHolder);
                //display choice using tracker 
                if (openChoice == JFileChooser.APPROVE_OPTION) {
                    //Put open file code in here
                    File openFile = fileDialog.getSelectedFile();
                    txtOutputPath.setText(openFile.getPath());
                    ideCardsHolder.setDirectoryPath(openFile.getPath());
                }
            }
        });
        pathPanel.add(btnSubmit, "align center,width 80!,height 25!");
        pathPanel.add(btnReset, "align center,width 80!,height 25!");
    }

    private void layoutComponents() {
        MigLayout mig = new MigLayout("insets 0", "[][]", "[]");
        setLayout(mig);
        contentPanel = new JPanel(new MigLayout("insets 0", "[]", "[]10[]10[]10[]"));
        contentPanel.add(lblOutPutPathDetails, "width 80%:85%:95%, height 7%!, growx,wrap");
        contentPanel.add(new JSeparator(), "grow,width 100%, wrap");
        contentPanel.add(pathPanel, "align center,width 50%:65%:70%, wrap, wrap");
        menuPanel = new MenuPanel();
        MenuPanelMouseEvent menuPanelMouseEvent = new MenuPanelMouseEvent(ideCardsHolder);
        menuPanel.getLblBasicConfiguraion().addMouseListener(menuPanelMouseEvent);
        add(menuPanel, "width 300!,height 100%");
        add(new JSeparator(JSeparator.VERTICAL), "growy");
        add(contentPanel, "align center,width 100%,height 100%");
    }

    public void updateListTableModel() {
        MigLayout mig = new MigLayout();
        tablepanel = new JPanel(new MigLayout("insets 0", "[]", "[]20[]20[]"));
        mig = new MigLayout("insets 10", "[]", "[][][]20[]");
        JComponent pageIndexPanel = updateTable();
        panelContent = new JPanel(mig);
        JScrollPane scrollTable = new JScrollPane();
        //  System.out.println("table data printing" + tableList.getName());
        scrollTable.setViewportView(tableList);
//        panelContent.add(lblMessage, "grow, wrap");
        lblSearch = new JLabel("Record Name");
        txtSearchTerm = new JTextField(15);
        mig = new MigLayout("insets 5", "[][grow]", "[]5[]");
        
        JPanel panelMsg = new JPanel();
        JLabel label = new JLabel("Please browse folder (or) drive");
        panelMsg.add(label);
        
        JPanel panelSearch = new JPanel(mig);
        panelSearch.add(lblSearch, "grow");
        panelSearch.add(txtSearchTerm, "grow, wrap,height 25!");
        panelSearch.add(new JLabel()); // leave a blank cell
        JPanel panelSearchOptions = new JPanel(new MigLayout("insets 0", "[][]", "[]"));
        panelSearch.add(panelSearchOptions, "grow");
        TitledBorder tBorder = BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.decode("#006699"), 1), "Search Records", TitledBorder.LEFT, TitledBorder.DEFAULT_POSITION, new Font("Tahoma", Font.BOLD, 13), Color.BLACK);
        panelSearch.setBorder(tBorder);
        txtSearchTerm.getDocument().addDocumentListener(new DocumentListener() {
        @Override
            public void insertUpdate(DocumentEvent e) {
                // updateRowFilters();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                //updateRowFilters();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                // updateRowFilters();
            }
        });
        JPanel btnPanel = new JPanel();
        btnPanel.add(btnMove, "width 80!,height 25!");
        if(this.getTxtOutputPath().getText()==null || this.getTxtOutputPath().getText()==""){
            panelContent.add(panelMsg, "wrap, grow");
            return;
        }
        panelContent.removeAll();
        panelContent.add(scrollTable, "grow, span 2, align center,width 70%:85%:95%, wrap");
        panelContent.add(btnPanel, "align right");
        panelContent.add(pageIndexPanel, "align left");
        panelContent.validate();
        tablepanel.removeAll();
        tablepanel.add(panelContent, "align center,alignx center,width 100%, growx, wrap");
        tablepanel.revalidate();
        contentPanel.add(tablepanel, "align center,alignx center,width 100%, growx, wrap");
        this.validate();
        this.setVisible(true);
    }
    
        private final JButton first = new JButton(new AbstractAction("|<") {
          @Override public void actionPerformed(ActionEvent e) {
            currentPageIndex = 1;
            initFilterAndButton();
          }
        });
        
        private final JButton prev  = new JButton(new AbstractAction("<") {
          @Override public void actionPerformed(ActionEvent e) {
            currentPageIndex -= 1;
            initFilterAndButton();
          }
        });
        
        private final JButton next = new JButton(new AbstractAction(">") {
          @Override public void actionPerformed(ActionEvent e) {
            currentPageIndex += 1;
            initFilterAndButton();
          }
        });
        
        private final JButton last = new JButton(new AbstractAction(">|") {
          @Override public void actionPerformed(ActionEvent e) {
            currentPageIndex = maxPageIndex;
            initFilterAndButton();
          }
        });
        
     private final JTextField field = new JTextField(2);
     private final JLabel label = new JLabel();
     
        private void initFilterAndButton() {
            sorter.setRowFilter(new RowFilter<TableModel, Integer>() {
              @Override public boolean include(RowFilter.Entry<? extends TableModel, ? extends Integer> entry) {
                int ti = currentPageIndex - 1;
                int ei = entry.getIdentifier();
                return ti*itemsPerPage<=ei && ei<ti*itemsPerPage+itemsPerPage;
              }
            });
            first.setEnabled(currentPageIndex>1);
            prev.setEnabled(currentPageIndex>1);
            next.setEnabled(currentPageIndex<maxPageIndex);
            last.setEnabled(currentPageIndex<maxPageIndex);
            field.setText(Integer.toString(currentPageIndex));
          }
    
    private final int itemsPerPage = 50;
    private int maxPageIndex;
    private int currentPageIndex = 1;     
        
        
    public JComponent updateTable() {
        JPanel panel = new JPanel();
        try{
                tableList = new JTable();
                String path = this.getTxtOutputPath().getText();
                fileslisttablemodel = new FilesListTableModel(path);
                DefaultTableModel model = fileslisttablemodel.getModel();
                tableList.setModel(model);
             //   tableList.getModel().addTableModelListener(this);
                tableList.getTableHeader().setFont(new Font("Tahoma", Font.BOLD, 12));
                tableList.setRowHeight(25);
                sorter = new TableRowSorter<>(fileslisttablemodel);
                tableList.setRowSorter(sorter);
        //        tableList.setAutoCreateRowSorter(true);    
                TableColumn column = null;
                int width = 0;
                for (int i = 0; i < tableList.getColumnCount(); i++) {
                    column = tableList.getColumnModel().getColumn(i);
                    switch (i) {
                        case 0:
                            width = 50;
                            break;
                        case 1:
                            width = 150;
                            break;
                        case 2:
                            width = 150;
                            break;
                        case 3:
                            width = 70;
                            break;
                        case 4:
                            width = 100;
                            break;
                        case 5:
                            width = 100;
                            break;
                    }
                    column.setPreferredWidth(width); 
                }
                JPanel po = new JPanel();
                po.add(field);
                po.add(label);
                JPanel box = new JPanel(new GridLayout(1,4,2,2));
                for(JComponent r:Arrays.asList(first, prev, po, next, last)) {
                  box.add(r);
                }
                int rowCount = model.getRowCount();
                int v = rowCount%itemsPerPage==0 ? 0 : 1;
                maxPageIndex = rowCount/itemsPerPage + v;
                initFilterAndButton();
                label.setText(String.format("/ %d", maxPageIndex));
                KeyStroke enter = KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0);
                field.getInputMap(JComponent.WHEN_FOCUSED).put(enter, "Enter");
                field.getActionMap().put("Enter", new AbstractAction() {
                  @Override public void actionPerformed(ActionEvent e) {
                    try {
                      int v = Integer.parseInt(field.getText());
                      if(v>0 && v<=maxPageIndex) {
                        currentPageIndex = v;
                      }
                    } catch(Exception ex) {
                      ex.printStackTrace();
                    }
                    initFilterAndButton();
                  }
                });
                DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
                rightRenderer.setHorizontalAlignment(JLabel.LEFT);
                for (int x = 2; x < tableList.getColumnCount(); x++) {
                    tableList.getColumnModel().getColumn(x).setCellRenderer(rightRenderer);
                }
                DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
                centerRenderer.setHorizontalAlignment(JLabel.LEFT);
                tableList.getColumnModel().getColumn(1).setCellRenderer(centerRenderer);
                panel.add(box);
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
        return panel;        
    }
   
   /**
    * Classes to handle rows and columns data within the table
    */ 
    private class RowListener implements ListSelectionListener {
        public void valueChanged(ListSelectionEvent event) {
            if (event.getValueIsAdjusting()) {
                return;
            }
            System.out.println("ROW SELECTION EVENT. ");
            outputSelection();
        }
    }
    
    private class ColumnListener implements ListSelectionListener {
        public void valueChanged(ListSelectionEvent event) {
            if (event.getValueIsAdjusting()) {
                return;
            }
            System.out.println("COLUMN SELECTION EVENT. ");
            outputSelection();
        }
    }
    
    private void outputSelection() {
        System.out.println(String.format("Lead: %d, %d. ",
                    tableList.getSelectionModel().getLeadSelectionIndex(),
                    tableList.getColumnModel().getSelectionModel().
                        getLeadSelectionIndex()));
        System.out.println("Rows:");
        for (int c : tableList.getSelectedRows()) {
            System.out.println(String.format(" %d", c));
        }
        System.out.println(". Columns:");
        for (int c : tableList.getSelectedColumns()) {
            System.out.println(String.format(" %d", c));
        }
        System.out.println(".\n");
    }

}
