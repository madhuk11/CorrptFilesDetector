/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.hcdc.cdac.ui.event;

import static in.hcdc.cdac.constants.Constants.CARD_DASHBOARD;
import in.hcdc.cdac.main.IDECardsHolder;
import in.hcdc.cdac.ui.OutputPathPanel;
import java.awt.Component;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author Madhusudhan <madhusudhankk@cdac.in>
 */
public class OutputPathPanelEvent implements ActionListener {

    private OutputPathPanel outputPathPanel;
    private final IDECardsHolder ideCardsHolder;
    

    public OutputPathPanelEvent(OutputPathPanel outputPathPanel, IDECardsHolder ideCardsHolder) {
        this.outputPathPanel = outputPathPanel;
        this.ideCardsHolder = ideCardsHolder;
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        if (e.getSource() == outputPathPanel.getBtnSave()) {
            String outputPath = outputPathPanel.getTxtOutputPath().getText();

            //  System.out.println(":: output path ::"+outputPath);
            outputPath = outputPath.replace('\\', '/');

            boolean flag = false;

            if (flag) {
                JOptionPane.showMessageDialog(outputPathPanel, "None of the fields can be empty or \n can have any of the following characters \\ | * : / ? < >");
                return;
            }
        } else if (e.getSource() == outputPathPanel.getBtnReset()) {
            outputPathPanel.getTxtOutputPath().setText("");
            outputPathPanel.getTxtOutputPath().setEnabled(true);
            outputPathPanel.getBtnBrowse().setEnabled(true);
            outputPathPanel.getBtnReset().setEnabled(false);
            outputPathPanel.getBtnSubmit().setEnabled(true);
            Component tablePanel = outputPathPanel.getTablepanel();
            outputPathPanel.getContentPanel().remove(tablePanel);
            outputPathPanel.repaint();
            outputPathPanel.validate();
            outputPathPanel.setVisible(true);
        } else if (e.getSource() == outputPathPanel.getBtnCancel()) {
            ideCardsHolder.getcLayout().show(ideCardsHolder, CARD_DASHBOARD);
        } else if (e.getSource() == outputPathPanel.getBtnSubmit()) {
            outputPathPanel.getBtnSubmit().setEnabled(false);
            outputPathPanel.getBtnReset().setEnabled(true);
            outputPathPanel.updateListTableModel();
        }
    }
    
    public  List<Component> getAllComponents(final Container c) {
        Component[] comps = c.getComponents();
        List<Component> compList = new ArrayList<Component>();
        for (Component comp : comps) {
            compList.add(comp);
            if (comp instanceof Container)
                compList.addAll(getAllComponents((Container) comp));
        }
        return compList;
    }
    
}
