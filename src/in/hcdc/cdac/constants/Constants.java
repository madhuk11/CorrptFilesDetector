/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package in.hcdc.cdac.constants;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import javax.swing.KeyStroke;

/**
 *
 * @author Madhusudhan <madhusudhankk@cdac.in>
 */
public class Constants {
    public static final String APPLICATION_NAME = "File Validate Tool";
    public static final String CARD_DASHBOARD = "startCard";
    public final static String MENU_ITEM_OTPUTPATH = "Output Path";
    public final static String MENU_ITEM_BASIC_CONFIGURATION = "Basic Configuration";
    public final static String MENU_BASIC_CONFIG = "Basic Configuration";
    public final static String MENU_ITEM_SHOW_DASHBOARD = "FVT Home";
    public final static String MENU_ITEM_HELP = "Help";
    public final static String MENU_ITEM_EXIT = "Exit";
    public static final KeyStroke KEY_SHOW_DASHBOARD = KeyStroke.getKeyStroke(KeyEvent.VK_H, ActionEvent.CTRL_MASK);//CTRL+H;
    public static final KeyStroke KEY_HELP = KeyStroke.getKeyStroke("F1");//F1
     //EXIT
    public static final KeyStroke KEY_EXIT = KeyStroke.getKeyStroke(KeyEvent.VK_F4, ActionEvent.ALT_MASK);//ALT+F4
}
