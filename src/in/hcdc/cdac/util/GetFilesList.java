package in.hcdc.cdac.util;

import com.lowagie.text.pdf.PdfReader;
import in.hcdc.cdac.detectfiles.CorruptedFileInfo;
import in.hdc.cdac.in.antiviruscheck.ClamAVFileScan;
import in.hdc.cdac.in.fileformatvalidation.JhoveInit;
import java.io.File;
import java.util.ArrayList;
import java.util.concurrent.Callable;

/**
 *
 * @author <madhusudhankk@cdac.in>
 *
 * @version 1
 * @since 1
 * @see in.hcdc.cdac.IndentifyCorruptedFiles
 */
public class GetFilesList implements Callable<ArrayList<CorruptedFileInfo>> {

    private ArrayList<CorruptedFileInfo> filesList = null;
    private File[] totalRecords;
    private double from;
    private double to;

    public GetFilesList(File[] totalRecords, double from, double to) {
        this.totalRecords = totalRecords;
        this.from = from;
        this.to = to;
        filesList = new ArrayList<>();
    }

    @Override
    public ArrayList<CorruptedFileInfo> call() throws Exception {
        // System.out.println("--total files --"+totalRecords.length);
        System.out.println("from: "+from+" to: "+to);
        for (double i = from; i <= to; i++) {
            int t = (int) i - 1;
            File file3 = totalRecords[t];
           // System.out.println("--t--"+t+" exists: "+file3.exists());
            if(file3.isFile()) {
          //          System.out.println("from: "+i+" to: "+to);
                    CorruptedFileInfo info = new CorruptedFileInfo();
                    String filename = file3.getName();
                    double fileSizeInMB = Helper.convertFilebyteToMB(file3);
                    info.setFileName(file3.getName());
                    info.setFilePath(file3.getAbsolutePath());
                    info.setFileSize(fileSizeInMB);
                    
                    /***
                     * Check whether file is well-formed and valid using Jhove
                     */
                    
                    
                    
                    JhoveInit ji = new JhoveInit();
                    boolean isvalid = ji.init(file3);
                    
                    if(isvalid){
                        info.setFileFormatValid("Ok");
                    }
                    else {
                        info.setFileFormatValid("Not well-formed");
                    }
                    
                    /***
                     * Check for virus on this file
                     */
                    ClamAVFileScan clamAVFileScan=new ClamAVFileScan(file3);
                    boolean isInfected = clamAVFileScan.scanResult();
                    
                    if(isInfected){
                         info.setVirusCheckStatus("No virus found");
                    }
                    else {
                        info.setVirusCheckStatus("Infected!!");
                    }
                    
                    if (file3.length() == 0) {
                         info.setFileFormatValid("Invalid");
                         
                    } else {
                        String[] split = filename.split("\\.");
                        String fileExtention = split[split.length - 1];
                        
                        if(fileExtention.equalsIgnoreCase("pdf")) {
                             PdfReader reader = null;
                                if (file3.exists()) {
                                    try {
                                        reader = new PdfReader(file3.getAbsolutePath());
                                       // info.setFileFormatValid("valid");
                                    } catch (Exception e) {
                                       // info.setFileFormatValid("invalid");
                                        if (reader != null) {
                                            reader.close();
                                        }
                                    }
                                }
                        }
                        else {
                           // info.setFileFormatValid("valid");
                        }
                    }
                    filesList.add(info);
            }
            else if(file3.isDirectory()){
                System.out.println(file3.getName()+"--dir");
            }
        }
      //  System.out.println("--added size--"+filesList.size());
        return filesList;
    }
}
